import React, {Component} from "react";
import {View} from 'react-native';
import {connect} from "react-redux";
import {HeaderBackButton} from "react-navigation";
import {axios} from 'axios';

import {setShowCameraComponent, clearShareImageScreenState} from "../actions/homepageactions";
import {PreviewImage} from "../components";

class ShareImageScreen extends Component {

    static navigationOptions = ({navigation}) => {
        const {params = {}} = navigation.state;
        return {
            headerLeft: (
                <HeaderBackButton title={'Home'} onPress={() => params.onBackButtonPress()}/>
            ),
        };
    };

    componentWillMount() {
        this.props.navigation.setParams({onBackButtonPress: this.onBackButtonPress});
    }

    checkInitialState = (props) => {
        return (props.showCameraComponent === false &&
            props.capturedImage === null &&
            props.navigateToHomeScreen === false);
    };

    componentWillReceiveProps(nextProps) {
        if (this.checkInitialState(nextProps)) {
            this.onBackButtonPress(false);
        }
    }

    onBackButtonPress = (clearState = true) => {
        if (clearState) {
            this.props.clearShareImageScreenState();
        }
        this.props.navigation.goBack();
    };

    render() {
        if (this.props.capturedImage !== null) {
            return <PreviewImage capturedImage={this.props.capturedImage}/>
        }
        else {
            return <View/>
        }
    }
}

function mapStateToProps(state) {
    return {
        showCameraComponent: state.homePageReducer.showCameraComponent,
        capturedImage: state.homePageReducer.capturedImage,
        navigateToHomeScreen: state.homePageReducer.navigateToHomeScreen
    }
}

export default connect(mapStateToProps, {
    setShowCameraComponent, clearShareImageScreenState
})(ShareImageScreen);