import React, { Component } from 'react';
import { connect } from "react-redux";
import AnimateNumber from 'react-native-animate-number';
import {CameraWithPermission} from "../components";

import {
    Platform,
    StyleSheet,
    Text,
    View,
    ImageBackground
} from 'react-native';

import {
    Button,
    Icon
} from 'react-native-elements';

import {
    Constants,
    Permissions
} from "expo";

import getLocationAsync from '../utils/util';

import {
    setUserLocation, updateImageCount, setShowCameraComponent
} from "../actions/homepageactions";

class HomeScreen extends Component {
    static navigationOptions = {
        header: null,
        tabBarLabel: 'Home',
        tabBarIcon: ({tintColor}) => {
            return <Icon name="home" size={30} color={tintColor}/>
        }
    };

    navigateToShareImageScreen = () => {
        this.props.navigation.navigate('share_image_screen');
    }

    onCameraButtonPress = () => {
        this.props.setShowCameraComponent();
    }

    async componentWillMount() {
        if (Platform.OS === 'android' && !Constants.isDevice) {
            this.setState({
                errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
            });
        } else {
            this.props.updateImageCount();

            let {status} = await Permissions.askAsync(Permissions.LOCATION);
            if (status !== 'granted') {
                this.setState({
                    errorMessage: 'Permission to access location was denied',
                });
            }
            else {
                let location = await getLocationAsync();
                this.props.setUserLocation(location);
            }
        }
    }

    shouldComponentUpdate(nextProp) {
        return (
            this.props.trashImageCount < nextProp.trashImageCount || this.props.showCameraComponent !== nextProp.showCameraComponent
        );
    }

    render() {
        if (this.props.showCameraComponent) {
            return <CameraWithPermission onImageCapture = {this.navigateToShareImageScreen}/>
        }

        return (
            <View style={styles.container}>
                <View style={styles.homeScreenTrashImageCountView}>
                    <AnimateNumber
                        style={styles.homeScreenTrashImageCountNumber}
                        value={this.props.trashImageCount}
                        formatter={(val) => {
                            return '' + parseInt(val)
                        }}
                        interval={1}
                        timing={(interval, progress) => {
                            // slow start, slow end
                            return interval * (1 - Math.sin(Math.PI * progress)) * 10
                        }}/>
                    <Text/>
                    <Text/>
                    <Text/>
                    <Text style={styles.homeScreenTrashImageCountText}>
                        Click photos of trash and share!
                    </Text>
                </View>

                <View style={styles.homeScreenCameraView}>
                    <Icon name="camera"
                          size={40}
                          underlayColor="#9E9E9E"
                          color="green"
                          raised={true}
                          onPress={() => this.onCameraButtonPress()}
                    />
                </View>
            </View>
        );
    }
}

function mapStateToProps({homePageReducer}) {
    return {
        showCameraComponent: homePageReducer.showCameraComponent,
        trashImageCount: homePageReducer.trashImageCount
    }
}

export default connect(mapStateToProps, {
    setUserLocation, updateImageCount, setShowCameraComponent
})(HomeScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    homeScreenMainTextView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    homeScreenMainText: {
        fontSize: 20,
        textAlign: 'center'
    },
    homeScreenTrashImageCountView: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    homeScreenTrashImageCountText: {
        fontSize: 20,
        textAlign: 'center',
        fontWeight: 'bold',
        color: 'green'

    },
    homeScreenTrashImageCountNumber: {
        fontWeight: 'bold',
        color: 'green',
        fontSize: 55,
    },
    homeScreenCameraView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    homeScreenCameraButton: {
        borderColor: "transparent",
        borderWidth: 2,
        borderRadius: 10
    }

});