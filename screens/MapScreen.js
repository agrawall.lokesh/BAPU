import React, {Component} from 'react';
import { Icon } from 'react-native-elements';

import TrashMap from '../components/Map';

export default class Home extends Component {
    static navigationOptions = {
        tabBarLabel: 'Map',
        tabBarIcon: ({ tintColor }) => {
            return <Icon name="my-location" size={30} color={tintColor} />;
        }
    }

    render() {
        return <TrashMap/>;
    }
}