import {combineReducers} from 'redux';
import homePageReducer from './homepagereducer';
import mapReducer from './mapreducer';
import locationReducer from './locationreducer';

export default combineReducers(
    {
        homePageReducer,
        mapReducer,
        locationReducer
    });