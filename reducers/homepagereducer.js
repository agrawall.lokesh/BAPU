import {
    SET_SHOW_CAMERA_COMPONENT,
    SET_SHOW_CAPTURED_IMAGE_AND_UNSET_CAMERA,
    CLEAR_SHARE_IMAGE_STATE,
    NAVIGATE_TO_HOME_SCREEN,
    UPDATE_IMAGE_COUNT,
    INCREMENT_IMAGE_COUNT
} from '../actions/types';

export default function (state = {
    showCameraComponent: false,
    capturedImage: null,
    capturedImageBase64: null,
    navigateToHomeScreen: false,
    trashImageCount: 0
}, action) {
    switch (action.type) {
        case SET_SHOW_CAMERA_COMPONENT:
            return {...state, showCameraComponent: true};
        case SET_SHOW_CAPTURED_IMAGE_AND_UNSET_CAMERA:
            return {
                ...state,
                capturedImage: action.image.uri,
                capturedImageBase64: action.image.base64,
                showCameraComponent: false
            };
        case CLEAR_SHARE_IMAGE_STATE:
            return {
                ...state,
                showCameraComponent: false,
                capturedImage: null,
                imageBase64: null,
                navigateToHomeScreen: true
            };
        case NAVIGATE_TO_HOME_SCREEN:
            return {
                ...state,
                showCameraComponent: false,
                capturedImage: null,
                imageBase64: null,
                navigateToHomeScreen: false
            };
        case UPDATE_IMAGE_COUNT:
            return {
                ...state,
                trashImageCount: action.trashImageData.data.imageCount
            };
        case INCREMENT_IMAGE_COUNT:
            return {
                ...state,
                trashImageCount: state.trashImageCount + 1
            };
        default:
            return state;
    }
}