import {MAP_POINTS} from '../actions/types';

const INITIAL_STATE = {
    markers: []
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case MAP_POINTS:
            return action.payload;
        default:
            return state
    }
}
