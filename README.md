# What is BAPU - Better Administration for People's Utilities ?

Citizens often commute on roads that have pot holes or open man holes or pass by garbage bins that need clearing. To connect citizens to municipal authorities enter a mobile app called BAPU.

BAPU has a simple interface to take photos of roads, man holes and other public amenities that require municipal action. Users can click a photo and a geotagged photo is posted to municipal authorities twitter or facebook sites. Other users can vote or post their own photos of areas nearby. Photos in a municipal region are shown as albums and municipalities will be alerted to address the issue depending on the severity reported.

This builds accountability and empowers citizens.

# Want to run this App?

## Running app from expo hosted server

This app is hosted in expo cloud https://expo.io/@agrawalo/bapu . 

1. Download expo client in your android/ios device. https://itunes.apple.com/us/app/expo-client/id982107779?mt=8 or https://play.google.com/store/apps/details?id=host.exp.exponent&hl=en 
2. Either scan barcode from https://expo.io/@agrawalo/bapu in the expo client app installed in your phone or type @agrawalo/bapu inside Expo Client, this will also open this project.

## Running app locally

This app can also be run locally by cloning this repo and starting local expo server.

1. Make sure you have node installed https://nodejs.org/en/download/   
2. Run “npm install -g create-react-native-app”
3. Run "git clone https://gitlab.com/agrawall.lokesh/BAPU.git"
4. cd BAPU
5. Run "npm install" (installs all the dependencies)
5. Run “npm start” (Starts expo server, and publishes QR code to scan from expo app/client installed in real device) -> expo server and expo client should be a part of same network.
 
Details http://facebook.github.io/react-native/docs/getting-started.html

# App Screenshots: 
https://drive.google.com/drive/folders/1rK7N8q9MxMbe-PGmW9ywqLIIx4xMI5QC?usp=sharing 

# Video URL:
https://vimeo.com/259491549 

