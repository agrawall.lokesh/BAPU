import {ImagePicker} from 'expo';
import React, {Component} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';

import {
    setShowCapturedImageAndUnsetShowCameraComponent,
    navigateToHomeScreen
} from '../actions/homepageactions';

class Camera extends Component {

    renderCamera = async function () {
        let imagePickerRes = await ImagePicker.launchCameraAsync({
            base64: true
        });

        // If image is captured
        if (!imagePickerRes.cancelled) {
            this.props.onImageCapture();
            this.props.setShowCapturedImageAndUnsetShowCameraComponent(imagePickerRes.uri, imagePickerRes.base64);
        }
        else {
            this.props.navigateToHomeScreen();
        }
    };

    render() {
        let _ = this.renderCamera();
        return <View/>;
    }
}

export default connect(null, {
    setShowCapturedImageAndUnsetShowCameraComponent, navigateToHomeScreen
})(Camera);
