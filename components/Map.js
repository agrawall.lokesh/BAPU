import React from 'react';
import { connect } from 'react-redux';
import { MapView } from 'expo';

import {
    StyleSheet,
    Dimensions,
    View,
    Button
} from 'react-native';


import mapStyle from '../assets/mapstyle.json';
import dustbinImg from '../assets/dustbin-red.png';
import { fetchMapPoints } from '../actions/mapactions'

const {
    width,
    height
} = Dimensions.get('window');

const ASPECT_RATIO = width / height;

const LATITUDE_DELTA = 18.0;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class TrashMap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            region: null,
            errorMessage: "",
        };
    }

    componentWillMount() {
        const {coords} = this.props.location;
        this.setState({
            region: {
                latitude: coords.latitude,
                longitude: coords.longitude,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            }
        });
        this.props.fetchMapPoints();
    }

    onButtonPress = () => {
        //precomputation over here
        this.props.fetchMapPoints();
    };

    renderTrashMarkers = () => {
        return this.props.markers.map((marker, index) => {
            return (
                <MapView.Marker
                    key={index}
                    coordinate={marker.coordinates}
                    image={dustbinImg}
                    title="1"
                >
                </MapView.Marker>
            );
        })
    }

    render() {
        if (this.state.region) {
            return (
                <View style={styles.container}>
                    <MapView
                        style={styles.map}
                        initialRegion={this.state.region}
                        customMapStyle={mapStyle}
                    >
                        {this.renderTrashMarkers()}
                    </MapView>
                    <View style={styles.buttonContainer}>
                        <Button
                            large
                            title="Refresh"
                            backgroundColor="#009688"
                            icon={{name: 'refresh'}}
                            onPress={this.onButtonPress}
                        />
                    </View>

                </View>
            );
        }
        else {
            return <View/>;
        }
    }
}

function mapStateToProps(state) {
    return {
        markers: state.mapReducer.markers,
        location: state.locationReducer.location
    };
}

export default connect(mapStateToProps, {fetchMapPoints})(TrashMap);


const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    bubble: {
        backgroundColor: 'rgba(255,255,255,0.7)',
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 20,
    },
    latlng: {
        width: 200,
        alignItems: 'stretch',
    },
    button: {
        width: 80,
        paddingHorizontal: 12,
        alignItems: 'center',
        marginHorizontal: 10,
    },
    buttonContainer: {
        flexDirection: 'row',
        marginVertical: 20,
        backgroundColor: 'transparent',
    },
});
