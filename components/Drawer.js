import {TabNavigator, StackNavigator} from 'react-navigation';

import HomeScreen from '../screens/HomeScreen';
import Map from '../screens/MapScreen';
import ShareImageScreen from '../screens/ShareImageScreen';

const Drawer = TabNavigator({
    home: StackNavigator({
        home_screen: {screen: HomeScreen},
        share_image_screen: {screen: ShareImageScreen}
    }),
    Map: {screen: Map}
}, {
    tabBarPosition: 'bottom',
    tabBarOptions: {
        labelStyle: {fontSize: 12}
    }
});

export default Drawer;