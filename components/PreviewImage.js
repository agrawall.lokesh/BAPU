import React, {Component} from 'react';
import { connect } from "react-redux";
import axios from "axios";
import Geocoder from 'react-native-geocoding';
import { Icon } from 'react-native-elements';
import querystring from "querystring";

import {
    StyleSheet,
    View,
    Image,
    Share
} from 'react-native';

import {updateImageCount, incrementImageCount, navigateToHomeScreen} from "../actions/homepageactions";

class PreviewImage extends Component {

    uploadToMongo = async (imageUrl) => {
        let uploadUrl = 'https://trashcandev.herokuapp.com/uploadTrashImage';
        let headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        };

        let _ = await axios.post(
            uploadUrl,
            querystring.stringify({
                latitude: this.props.location.coords.latitude,
                longitude: this.props.location.coords.longitude,
                imageUrl: imageUrl
            }),
            headers);
        this.props.updateImageCount()
    };

    uploadToS3 = async () => {
        let uploadUrl = 'https://trashcannodeserver.herokuapp.com/upload';
        let headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        };
        let response = await axios.post(uploadUrl, {photo: this.props.capturedImageBase64}, headers);
        this.uploadToMongo(response.data);
    };

    getLocationFromObjects = (location) => {
        const locality = location.map((x) => {
            return x.long_name
        });

        const address = locality.join('');
        const addressWithoutSpaces = address.replace(/\s+/g,"");
        return "#BAPU "+ "#" + addressWithoutSpaces;
    };

    onShareClick = async () => {
        let lat = this.props.location.coords.latitude;
        let lon = this.props.location.coords.longitude;
        const API_KEY = 'AIzaSyCl7YViIrmuNb18A4W1rX4hDC2bD_lW91w';

        Geocoder.setApiKey(API_KEY);
        Geocoder.getFromLatLng(lat, lon).then(
            json => {
                let address_component = json.results[0].address_components[0];

                let location_string = this.getLocationFromObjects(json.results[0].address_components);

                // alert(location_string);

                Share.share({
                        message: location_string,
                        title: 'share',
                        url: this.props.capturedImage
                    }, {
                        dialogTitle: 'This is share dialog title',
                        tintColor: 'green'
                    }
                ).then(() => {
                    this.props.incrementImageCount();
                    this.props.navigateToHomeScreen();
                    this.uploadToS3();
                }).catch((err) => console.log(err));
            },
            error => {
                alert(error);
            }
        );
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.shareImageCapturedImage}>
                    <Image resizeMode="cover"
                           style={styles.previewImage}
                           source={{uri: this.props.capturedImage}}/>
                </View>
                <View style={styles.shareImageButton}>
                    <Icon name="share"
                          type='feather'
                          size={40}
                          underlayColor="#9E9E9E"
                          color="green"
                          raised={true}
                          onPress={() => this.onShareClick()}
                    />
                </View>
            </View>
        )
    }
}


function mapStateToProps({homePageReducer, locationReducer}) {
    return {
        capturedImage: homePageReducer.capturedImage,
        capturedImageBase64: homePageReducer.capturedImageBase64,
        location: locationReducer.location
    }
}

export default connect(mapStateToProps,
    {updateImageCount, incrementImageCount, navigateToHomeScreen})(PreviewImage);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    shareImageCapturedImage: {
        flex: 2,
        margin: 10,
        padding: 10,
        borderColor: "black",
        borderWidth: 1,
        width: undefined,
        height: undefined
    },
    previewImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    shareImageButton: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});