export const SET_SHOW_CAMERA_COMPONENT = 'set-show-camera-component';
export const SET_SHOW_CAPTURED_IMAGE_AND_UNSET_CAMERA = 'set-show-captured-image-and-unset-camera';
export const CLEAR_SHARE_IMAGE_STATE = 'clear-share-image-state';
export const NAVIGATE_TO_HOME_SCREEN = 'navigate-to-home-screen';
export const MAP_POINTS = 'map_points';
export const USER_LOCATION = 'user-location';
export const UPDATE_IMAGE_COUNT = 'update-image-count';
export const INCREMENT_IMAGE_COUNT = 'increment-image-count';
