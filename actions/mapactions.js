import axios from 'axios';

const MAP_URL = 'http://trashcandev.herokuapp.com/getTrashImages';
import {MAP_POINTS} from './types';

export const fetchMapPoints = () => async (dispatch) => {
    let {data} = await axios.get(MAP_URL);

    const payload = data.map((x) => {
        return {coordinates: {latitude: x.location.coordinates[0], longitude: x.location.coordinates[1]},imageUrl:x.imageUrl}
    });

    dispatch({
        type: MAP_POINTS,
        payload: {"markers": payload}
    });
};



