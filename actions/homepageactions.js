import {
    SET_SHOW_CAMERA_COMPONENT,
    SET_SHOW_CAPTURED_IMAGE_AND_UNSET_CAMERA,
    NAVIGATE_TO_HOME_SCREEN,
    CLEAR_SHARE_IMAGE_STATE,
    USER_LOCATION,
    UPDATE_IMAGE_COUNT,
    INCREMENT_IMAGE_COUNT
} from './types';
import axios from "axios/index";

const TRASH_IMAGE_COUNT_URL = 'http://trashcandev.herokuapp.com/getTrashImageCount';

export const setShowCameraComponent = () => {
    return {
        type: SET_SHOW_CAMERA_COMPONENT
    }
};

export const setShowCapturedImageAndUnsetShowCameraComponent = (imageUri, imageBase64) => {
    return {
        type: SET_SHOW_CAPTURED_IMAGE_AND_UNSET_CAMERA,
        image: {uri: imageUri, base64: imageBase64}
    }
};

export const navigateToHomeScreen = () => {
    return {
        type: NAVIGATE_TO_HOME_SCREEN
    }
};

export const clearShareImageScreenState = () => {
    return {
        type: CLEAR_SHARE_IMAGE_STATE
    }
};

export const setUserLocation = (location) => {
    return {
        type: USER_LOCATION,
        location: location
    }
};

export const updateImageCount = () => {
    return async (dispatch) => {
        let data = await axios.get(TRASH_IMAGE_COUNT_URL);
        dispatch({
            type: UPDATE_IMAGE_COUNT,
            trashImageData: data
        });
    }
};

export const incrementImageCount = () => {
    return {
        type: INCREMENT_IMAGE_COUNT
    }
};
