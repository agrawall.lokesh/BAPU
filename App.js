import React from 'react';
import {Provider} from 'react-redux';

import Drawer from './components/Drawer';
import store from './store';

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Drawer/>
            </Provider>
        );
    }
}