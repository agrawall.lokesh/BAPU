import {
    Location
} from "expo";

export default getLocationAsync = async () => {
    return await Location.getCurrentPositionAsync({});
};